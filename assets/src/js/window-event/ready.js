// Windows Ready Handler

(function($) {

    $(document).ready(function(){

      	sticky_header_initialize();

      	//Mobile Toggle
        $('.mobile-toggle').on('click', function(){
    		$('.mega-menu-toggle').toggleClass('mega-menu-open');
    		console.log('clicked');
    	});
        
    });

}(jQuery));
function sticky_header_initialize(){
	var headerHeight = jQuery('.site-header').outerHeight();
	var utility = jQuery('.utility-bar').outerHeight();
	var header = jQuery('.header-main').outerHeight();
	jQuery('.site-content').css('marginTop', headerHeight);

	if(jQuery(window).width() < 769){
		jQuery('nav.navbar').css('max-height', 'calc( 100vh - '+ headerHeight +'px )');
	}else{
		jQuery('nav.navbar').css('max-height', 'auto');
	}
}

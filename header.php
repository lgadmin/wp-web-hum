<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */
 
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

	<header id="masthead" class="site-header sticky-header">
    <div class="header-content">
      <div class="utility-bar">
        <div class="container-lg">
          <div class="left">
            <?php echo do_shortcode('[lg-social-media]'); ?>
            <div class="header-address">
              <i class="fa fa-map-marker" aria-hidden="true"></i> <span><?php echo do_shortcode('[lg-address1]'); ?> <?php echo do_shortcode('[lg-address2]'); ?>, <?php echo do_shortcode('[lg-city]'); ?>, <?php echo do_shortcode('[lg-province]'); ?></span>
            </div>
          </div>
          <div class="right">
            <div>
              <a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><i class="fa fa-envelope" aria-hidden="true"></i> <span><?php echo do_shortcode('[lg-email]'); ?></span></a>
            </div>
            <div>
              <a href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><i class="fa fa-phone" aria-hidden="true"></i> <span><?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></span></a>
            </div>
          </div>
        </div>
      </div>
      <div class="header-main container-lg">
    		<div class="site-branding">
    			<div class="logo">
    				<?php
    					if(shortcode_exists('lg-site-logo')){
    						echo do_shortcode('[lg-site-logo]');
    					}else{
    						the_custom_logo();
    					}
    			 	?>
    			</div>

          <div class="mobile-toggle"><i class="fa fa-bars" aria-hidden="true"></i></div>
    		</div><!-- .site-branding -->

        <div class="main-navigation">
          	<nav class="navbar navbar-default">  
          	    <!-- Brand and toggle get grouped for better mobile display -->
          	    <div class="navbar-header">
          	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
          	        <span class="sr-only">Toggle navigation</span>
          	        <span class="icon-bar"></span>
          	        <span class="icon-bar"></span>
          	        <span class="icon-bar"></span>
          	      </button>
          	    </div>


                <!-- Main Menu  -->
                <?php 

                    echo do_shortcode('[maxmegamenu location=top-nav]');

                ?>
          	</nav>
        </div>
      </div>
    </div>

  </header><!-- #masthead -->

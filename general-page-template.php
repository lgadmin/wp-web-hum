<?php
/**
 * Template Name: Page Template
 *
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area general">
		<main id="main" class="site-main">

		<!-- Top Banner -->
		<?php get_template_part("/templates/template-parts/top-banner"); ?>
		<!-- end Top Banner -->

		<!-- Content -->
		<div class="block pt-lg pb-lg main-content">
			<div class="container">
				<?php
				while ( have_posts() ) : the_post();
					the_content();
				endwhile; // End of the loop.
				?>
			</div>
		</div>
		<!-- end Content -->

		<!-- Testimonials -->
		<?php get_template_part("/templates/template-parts/testimonials"); ?>
		<!-- end Testimonials -->

		<!-- Events -->
		<?php get_template_part("/templates/template-parts/upcoming-events"); ?>
		<!-- end Events -->

		</main>
	</div>
</div>
<?php get_footer();

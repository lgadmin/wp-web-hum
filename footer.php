<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	<footer id="colophon" class="site-footer">
		<div id="site-footer" class="bg-gray-dark clearfix pb-md pt-md">
			<div class="container-lg">
				<div class="nav-footer"><?php get_template_part("/templates/template-parts/nav-footer"); ?></div>
				<div class="twitter-feed"><?php get_template_part("/templates/template-parts/twitter-feed"); ?></div>
				<div class="company-news"><?php get_template_part("/templates/template-parts/company-news"); ?></div>
				<div class="footer-utility"><?php get_template_part("/templates/template-parts/address-card"); ?></div>
			</div>
		</div>
		<div id="site-legal" class="bg-gray-base block pb-xs pt-xs clearfix">
			<div class="container-lg">
				<div class="site-info"><?php get_template_part("/templates/template-parts/site-info"); ?></div>
				<div class="site-longevity"> <?php get_template_part("/templates/template-parts/site-footer-longevity"); ?> </div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

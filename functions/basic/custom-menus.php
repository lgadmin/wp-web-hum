<?php

function create_menu() {
    add_menu_page(
        __( 'HUM', 'hum' ),     ### Need change
        'HUM',        ### Need change
        'edit_posts',
        'hum',   ### Need change
        '',
        'dashicons-admin-site',
        2
    );

    add_submenu_page(
        'hum',
        __('Client Instruction'), 
        __('Client Instruction'), 
        'edit_posts', 
        'theme_cms', 
        'client_instruction'
    );

    if( function_exists('acf_add_options_page') ) {
		$option_page = acf_add_options_page(array(
			'page_title' 	=> 'Global Theme Options',
			'menu_title' 	=> 'Global Theme Options',
			'menu_slug' 	=> 'global-theme-options',
			'parent_slug'	=> 'hum',
			'capability' 	=> '',
			'redirect' 	=> false
		));
	}
}

add_action( 'admin_menu', 'create_menu' );

function client_instruction(){
    require_once(get_stylesheet_directory() . '/functions/basic/client-instruction.php');
}

?>
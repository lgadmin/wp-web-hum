<style>
	ul{
		list-style:default;
		padding-left:20px;
	}
</style>

<div>
	<h1>Client Instruction</h1>

	<div style="padding: 15px; background-color:#fff;">
		
		<div>
			<h2>Site General</h2>
			<ul>
				<li><b>Logo:</b> LG Theme -> Site Settings -> Logo</li>
				<li><b>Social Media:</b> LG Theme -> Social Media</li>
				<li><b>Contact Info:</b> Dashboard-> LG Theme -> Contact</li>
				<li><b>Tracking Scripts:</b> LG Theme -> Analytics Tracking (Google tag manager setup under Longevity Account, No code injection to the header)</li>
			</ul>
		</div>

		<hr>

		<div>
			<h2>Pages</h2>
			<ul>
				<li><b>Home:</b> Pages -> Home</li>
				<li><b>About:</b> Pages -> About
					<ul style="margin:15px 0;">
						<li><b>Integrated Continuing Care Framework:</b> Pages -> Integrated Continuing Care Framework</li>
						<li><b>Policies and Fees:</b> Pages -> Policies and Fees</li>
						<li><b>HUM Events and Workshops:</b> Pages -> HUM Events and Workshops</li>
					</ul>
				</li>
				<li><b>Financial Assistance:</b> Pages -> Financial Assistance</li>
				<li><b>FAQ:</b> Pages -> Frequently Asked Questions</li>
				<li><b>Contact:</b> Pages -> Contact</li>
			</ul>
		</div>

		<hr>

		<div>
			<h2>Services</h2>
			<p>Services have 5 categories. They are <b>Addiction Service</b>, <b>Mental Health Service</b>, <b>Personal Service</b>, <b>Corporate Service</b>, and <b>Counselling Service</b>. Please categirized them correctly so that the website frontend will update accordingly. One service can have multiple categories. Please take a look at some examples.</p>
			<ul>
				<li>HUM -> Services</li>
			</ul>
		</div>

		<hr>

		<div>
			<h2>Resources</h2>
			<p>*Resources for both Addiction and Mental Health</p>
			<ul>
				<li>HUM -> Resources</li>
			</ul>
		</div>

		<hr>

		<div>
			<h2>Events</h2>
			<p>Start Date is required, End date can be optional. Events datetime has 3 types
				<ul style="margin: 15px 0;">
					<li>All Day: will display 'all day' in the frontend</li>
					<li>Varies: will display 'time varies' in the frontend</li>
					<li>Time Range: 2 more fields will shows up if you select this option, whichs are 'Start Time' and 'End Time'</li>
				</ul>

				*If the location is HUM location. Don't need to change. If not, please change it.
			</p>
			<ul>
				<li>HUM -> Events</li>
			</ul>
		</div>

		<hr>

		<div>
			<h2>Team Members</h2>
			<ul>
				<li>HUM -> Team Members</li>
			</ul>
		</div>

		<hr>

		<div>
			<h2>Associates</h2>
			<ul>
				<li>HUM -> Associates</li>
			</ul>
		</div>

		<hr>

		<div>
			<h2>Careers</h2>
			<ul>
				<li>HUM -> Careers</li>
			</ul>
		</div>

		<hr>

		<div>
			<h2>Global Theme Options</h2>
			<ul>
				<li>HUM -> Global Theme Options</li>
				<ul style="margin: 15px 0;">
					<li>Top image banner for all the pages</li>
					<li>Financial Available image and text</li>
					<li>Services page top banner text and form</li>
				</ul>
			</ul>
		</div>


	</div>

</div>
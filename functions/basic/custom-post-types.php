<?php

//Custom Post Types
function create_post_type() {

    // SERVICES
    register_post_type( 'service',
        array(
          'labels' => array(
            'name' => __( 'Services' ),
            'singular_name' => __( 'Service' )
          ),
          'public' => true,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'hum',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor' )
        )
    );

    // RESOURCES
    register_post_type( 'resource',
        array(
          'labels' => array(
            'name' => __( 'Resources' ),
            'singular_name' => __( 'Resource' )
          ),
          'public' => true,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'hum',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor' )
        )
    );

    // EVENT
    register_post_type( 'event',
        array(
          'labels' => array(
            'name' => __( 'Events' ),
            'singular_name' => __( 'Event' )
          ),
          'public' => true,
          'has_archive' => true,
          'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'hum',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor' )
        )
    );

    // Team
    register_post_type( 'team',
        array(
          'labels' => array(
            'name' => __( 'Team Members' ),
            'singular_name' => __( 'Team Member' )
          ),
          'public' => true,
          'has_archive' => true,
          'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'hum',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor' )
        )
    );

    // Member
    register_post_type( 'associate',
        array(
          'labels' => array(
            'name' => __( 'Associates' ),
            'singular_name' => __( 'Associate' )
          ),
          'public' => true,
          'has_archive' => true,
          'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'hum',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor' )
        )
    );

    // Career
    register_post_type( 'career',
        array(
          'labels' => array(
            'name' => __( 'Careers' ),
            'singular_name' => __( 'Career' )
          ),
          'public' => true,
          'has_archive' => true,
          'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'hum',   #### Main menu slug
          'supports' => array( 'title', 'editor' )
        )
    );

}
add_action( 'init', 'create_post_type' );

?>
<?php

function create_taxonomy(){

	register_taxonomy(
		'service-category',
		'service',
		array(
			'label' => __( 'Service Category' ),
			'rewrite' => array( 'slug' => 'service-category' ),
			'hierarchical' => true,
		)
	);

	register_taxonomy(
		'resource-category',
		'resource',
		array(
			'label' => __( 'Resource Category' ),
			'rewrite' => array( 'slug' => 'resource-category' ),
			'hierarchical' => true,
		)
	);

	register_taxonomy(
		'member-category',
		'member',
		array(
			'label' => __( 'Member Category' ),
			'rewrite' => array( 'slug' => 'member-category' ),
			'hierarchical' => true,
		)
	);
}

add_action( 'init', 'create_taxonomy' );

?>
<?php
	$we_can_help_content = get_field('we_can_help_content', 'option');
	$we_can_help_form = get_field('we_can_help_form', 'option');
?>
<div class="pt-xs pb-xs service-top-banner bg-green">
	<div class="container">
		<div>
			<h2 class="h2 non-visible">
				placeholder
			</h2>
			<?php echo $we_can_help_content; ?>
		</div>
		<div class="center">
			<h2 class="h2">We Can Help</h2>
			<?php echo do_shortcode($we_can_help_form); ?>
		</div>
	</div>
</div>
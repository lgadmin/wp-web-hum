<?php
	$args = array(
        'showposts'	=> -1,
        'post_type'		=> 'service',
        'tax_query' => array(
			array(
				'taxonomy' => 'service-category',
				'field'    => 'slug',
				'terms'    => array('addiction-services')
			),
		),
    );
    $result = new WP_Query( $args );

    // Loop
    if ( $result->have_posts() ) :
    	?>
    	<div class="services-grid">
    	<?php
        while( $result->have_posts() ) : $result->the_post();
            $link = get_permalink();
    	?>
    		<div>
    			<a href="<?php echo $link; ?>"><?php the_title(); ?></a>
    		</div>
		<?php
        endwhile;
        ?>
        </div>
        <?php
    endif; // End Loop

    wp_reset_query();
?>
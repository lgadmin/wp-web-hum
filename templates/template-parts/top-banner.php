<?php
	$featured_banner_default_background_image = get_field('featured_banner_default_background_image', 'option');
	$featured_banner_height = get_field('featured_banner_height', 'option');
?>
<div class="top-banner" style="background-image: url('<?php echo $featured_banner_default_background_image; ?>'); height: <?php echo $featured_banner_height; ?>px;">

	<?php if(is_tax()): ?>
		<h2 class="h1"><?php single_term_title(); ?></h2>
	<?php elseif(is_archive()): ?>
		<h2 class="h1"><?php echo get_the_archive_title(); ?></h2>
	<?php else: ?>
		<h2 class="h1"><?php echo get_the_title(); ?></h2>
	<?php endif; ?>
</div>
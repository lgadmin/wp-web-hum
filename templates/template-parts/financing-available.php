<?php
	$financing_image = get_field('financing_image', 'option');
	$financing_title = get_field('financing_title', 'option');
	$financing_description = get_field('financing_description', 'option');
	$financing_link = get_field('financing_link', 'option');
?>

<div class="financing pt-lg pb-lg bg-gray-lighter">
	<div class="container">
		<div class="split-image">
			<img src="<?php echo $financing_image['url']; ?>" alt="<?php echo $financing_image['alt']; ?>">
		</div>
		<div class="split-copy">
			<?php if($financing_title): ?>
				<h2 class="h2"><?php echo $financing_title; ?></h2>
			<?php endif ;?>
			<?php echo $financing_description; ?>
			<a href="<?php echo $financing_link->guid; ?>" class="cta">Learn more</a>
		</div>
	</div>
</div>
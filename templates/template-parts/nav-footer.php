<?php 

  $mainMenu = array(
  	'menu'              => 'footer-navigation',
  	// 'theme_location'    => 'top-nav',
  	'depth'             => 1,
  	'container'         => 'div',
  	'container_class'   => 'collapse navbar-collapse',
  	'menu_class'        => 'nav navbar-nav',
  	'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
  		// 'walker'         => new WP_Bootstrap_Navwalker_Custom()  // Custom used in Skin Method.
  	'walker'            => new WP_Bootstrap_Navwalker()
  );
  wp_nav_menu($mainMenu);

?>
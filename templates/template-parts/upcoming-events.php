<div class="upcoming-events pt-lg pb-lg center">
	<div class="container">
		<h2 class="h2">Upcoming Events</h2>
		<p>Join us for one of our upcoming events! <a href="/event/?filter=upcoming">See all upcoming events</a></p>
		<?php
			$args = array(
		        'showposts'	=> -1,
		        'post_type'	=> 'event',
		        'meta_key'			=> 'start_date',
				'orderby'			=> 'meta_value',
				'order'				=> 'ASC'
		    );
		    $result = new WP_Query( $args );

		    // Loop
		    if ( $result->have_posts() ) :
		    	?>
		    	<div class="events pt-sm">
		    	<?php
		        while( $result->have_posts() ) : $result->the_post();
		        	$location = get_field('location');
		        	$date_type = get_field('date_type');
		        	$start_date = get_field('start_date', false, false);
		        	$end_date = get_field('end_date', false, false);
		        	$start_time = get_field('start_time');
		        	$end_time = get_field('end_time');
		        	$event_description = get_field('event_description');

		        	$upcoming = false;

		        	date_default_timezone_set('UTC');
		        	$current_datetime = new DateTime();
		        	$start_datetime = new DateTime($start_date);		        	

		        	if($end_date){
		        		$end_datetime = new DateTime($end_date);
		        		$upcoming = $current_datetime <= $end_datetime;
		        	}else{
		        		$upcoming = $current_datetime <= $start_datetime;
		        	}
		    	?>

		    	<?php if($upcoming): ?>
		    		<div>
						<div class="date">
							<div class="start">
								<div class="day">
									<div class="day-of-week">
										<?php echo $start_datetime->format( 'D' ); ?>
									</div>
									<div class="day-of-month">
										<?php echo $start_datetime->format( 'd' ); ?>
									</div>
								</div>
								<div class="year-month">
									<?php echo $start_datetime->format( 'M' ); ?>
									<br>
									<?php echo $start_datetime->format( 'Y' ); ?>
								</div>
							</div>
							<?php if($end_date): ?>
							<div class="end">
								<div class="day">
									<div class="day-of-week">
										<?php echo $end_datetime->format( 'D' ); ?>
									</div>
									<div class="day-of-month">
										<?php echo $end_datetime->format( 'd' ); ?>
									</div>
								</div>
								<div class="year-month">
									<?php echo $end_datetime->format( 'M' ); ?>
									<br>
									<?php echo $end_datetime->format( 'Y' ); ?>
								</div>
							</div>
						<?php endif; ?>
						</div>
						<div class="content">
							<h3 class="h4"><?php the_title(); ?></h3>
							<div class="meta">
								<?php
									if($date_type == 'All Day' || $date_type == 'Varies'){
										echo $date_type;
									}else if($date_type == 'Time Range'){
										if( $start_time && $end_time ){
											echo $start_time . ' - ' . $end_time;
										}else if( !$start_time && $end_time ){
											echo 'Before ' . $end_time;
										}else if( $start_time && !$end_time ){
											echo 'After ' . $start_time;
										}
									}
								?>
								<br>
								<?php echo do_shortcode($location); ?>
							</div>
							<?php echo short_string($event_description, 10); ?>
							<a class="learn-more" href="<?php echo get_permalink(); ?>">Read More</a>
						</div>
		    		</div>
		    	<?php endif; ?>
				<?php
		        endwhile;
		        ?>
		        </div>
		        <?php
		    else:
		    	?>
				No upcoming event found.
		    	<?php
		    endif; // End Loop

		    wp_reset_query();
		?>
	</div>
</div>
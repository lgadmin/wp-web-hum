<?php
/**
 * Default Page Template. Fixed width, no sidebar
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area single-resource">
		<main id="main" class="site-main">

			<!-- Top Banner -->
			<?php get_template_part("/templates/template-parts/top-banner"); ?>
			<!-- end Top Banner -->

			<!-- Top Form -->
			<?php get_template_part("/templates/template-parts/service-top-form"); ?>
			<!-- end Top Form -->

			<!-- Content -->
			<?php setup_postdata($post) ?>

			<div class="block container main-content pt-lg pb-lg">
				<?php the_content(); ?>
			</div>
			<!-- end Content -->

			<!-- Financing Section -->
			<?php get_template_part("/templates/template-parts/financing-available"); ?>
			<!-- end Financing Section -->

		</main>
	</div>
</div>
<?php get_footer();

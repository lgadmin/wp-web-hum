<?php
/**
 * Default Page Template. Fixed width, no sidebar
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area single-resource">
		<main id="main" class="site-main">

			<!-- Top Banner -->
			<?php get_template_part("/templates/template-parts/top-banner"); ?>
			<!-- end Top Banner -->

			<!-- Content -->
			<?php
				$location = get_field('location');
	        	$date_type = get_field('date_type');
	        	$start_date = get_field('start_date', false, false);
	        	$end_date = get_field('end_date', false, false);
	        	$start_time = get_field('start_time');
	        	$end_time = get_field('end_time');
	        	$event_description = get_field('event_description');
			?>

			<div class="block container main-content pt-lg pb-lg">
					<div class="center">Join us for one of our upcoming events! See all <a href="/event/?filter=upcoming">upcoming events</a></div>
					<div class="events event pt-sm">
				    	<?php
				        	$location = get_field('location');
				        	$date_type = get_field('date_type');
				        	$start_date = get_field('start_date', false, false);
				        	$end_date = get_field('end_date', false, false);
				        	$start_time = get_field('start_time');
				        	$end_time = get_field('end_time');
				        	$event_description = get_field('event_description');

				        	date_default_timezone_set('UTC');
				        	$current_datetime = new DateTime();
				        	$start_datetime = new DateTime($start_date);	        	

				        	if($end_date){
				        		$end_datetime = new DateTime($end_date);
				        		$end_year = $end_datetime->format( 'Y' );
				        	}
				        	//

				        	$start_year = $start_datetime->format( 'Y' );
				        	if(isset($_GET['filter'])){
				        		$filter = $_GET['filter'];
				        	}
				    	?>

				    		<div>
								<div class="date">
									<div class="start">
										<div class="day">
											<div class="day-of-week">
												<?php echo $start_datetime->format( 'D' ); ?>
											</div>
											<div class="day-of-month">
												<?php echo $start_datetime->format( 'd' ); ?>
											</div>
										</div>
										<div class="year-month">
											<?php echo $start_datetime->format( 'M' ); ?>
											<br>
											<?php echo $start_datetime->format( 'Y' ); ?>
										</div>
									</div>
									<?php if($end_date): ?>
									<div class="end">
										<div class="day">
											<div class="day-of-week">
												<?php echo $end_datetime->format( 'D' ); ?>
											</div>
											<div class="day-of-month">
												<?php echo $end_datetime->format( 'd' ); ?>
											</div>
										</div>
										<div class="year-month">
											<?php echo $end_datetime->format( 'M' ); ?>
											<br>
											<?php echo $end_datetime->format( 'Y' ); ?>
										</div>
									</div>
								<?php endif; ?>
								</div>
								<div class="content">
									<h3 class="h4"><?php the_title(); ?></h3>
									<div class="meta">
										<?php
											if($date_type == 'All Day' || $date_type == 'Varies'){
												echo $date_type;
											}else if($date_type == 'Time Range'){
												if( $start_time && $end_time ){
													echo $start_time . ' - ' . $end_time;
												}else if( !$start_time && $end_time ){
													echo 'Before ' . $end_time;
												}else if( $start_time && !$end_time ){
													echo 'After ' . $start_time;
												}
											}
										?>
										<br>
										<?php echo do_shortcode($location); ?>
									</div>
									<?php echo $event_description; ?>
								</div>
				    		</div>
				        </div>
			</div>
			<!-- end Content -->

			<!-- Financing Section -->
			<?php get_template_part("/templates/template-parts/financing-available"); ?>
			<!-- end Financing Section -->

		</main>
	</div>
</div>
<?php get_footer();

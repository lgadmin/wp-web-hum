<?php
/**
 * Default Page Template. Fixed width, no sidebar
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area single-service">
		<main id="main" class="site-main">

			<!-- Top Banner -->
			<?php get_template_part("/templates/template-parts/top-banner"); ?>
			<!-- end Top Banner -->

			<!-- Top Form -->
			<?php get_template_part("/templates/template-parts/service-top-form"); ?>
			<!-- end Top Form -->

			<!-- Content -->
			<?php setup_postdata($post) ?>

			<div class="block container main-content pt-lg pb-lg">
				<?php the_content(); ?>
			</div>
			<!-- end Content -->

			<!-- Services Grid -->
			<?php if(has_term( 'addiction-services', 'service-category' )): ?>
			<div class="addiction-service pb-lg center">
				<div class="container">
					<?php get_template_part("/templates/template-parts/grid-addiction-service"); ?>
				</div>
			</div>
			<?php endif; ?>

			<?php if(has_term( 'mental-health-services', 'service-category' )): ?>
			<div class="mental-service pb-lg center">
				<div class="container">
					<?php get_template_part("/templates/template-parts/grid-mental-health"); ?>
				</div>
			</div>
			<?php endif; ?>

			<?php if(has_term( 'corporate-service', 'service-category' )): ?>
			<div class="corporate-service pb-lg center">
				<div class="container">
					<?php get_template_part("/templates/template-parts/grid-corporate-service"); ?>
				</div>
			</div>
			<?php endif; ?>
			<!-- end Services Grid -->

			<!-- Financing Section -->
			<?php get_template_part("/templates/template-parts/financing-available"); ?>
			<!-- end Financing Section -->

		</main>
	</div>
</div>
<?php get_footer();

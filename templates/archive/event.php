<?php
/**
 * Default Page Template. Fixed width, no sidebar
 */

get_header(); 

?>
<div id="content" class="site-content">
	<div id="primary" class="content-area archive-event">
		<main id="main" class="site-main">

		<!-- Top Banner -->
		<?php get_template_part("/templates/template-parts/top-banner"); ?>
		<!-- end Top Banner -->

		<div class="archive-events pt-lg pb-lg center">
			<div class="container">
				<p>Join us for one of our upcoming events!</p>
				<?php
					$args = array(
				        'showposts'	=> -1,
				        'post_type'		=> 'event',
				    );
				    $result = new WP_Query( $args );
				    // Loop
				    if ( $result->have_posts() ) :
				    	$years = [];
				    	?>
				    	<?php while( $result->have_posts() ) : $result->the_post(); ?>
							<?php
								$start_date = get_field('start_date', false, false);
								$start_datetime = new DateTime($start_date);	
								$start_year = $start_datetime->format( 'Y' );
								$end_date = get_field('end_date', false, false);

								if(isset($_GET['filter'])){
									$filter = $_GET['filter'];		
								}

								if(!in_array($start_year, $years, true)){
							        array_push($years, $start_year);
							    }

								if($end_date){
					        		$end_datetime = new DateTime($end_date);
					        		$end_year = $end_datetime->format( 'Y' );
					        		if(!in_array($end_year, $years, true)){
								        array_push($years, $end_year);
								    }
					        	}
							?>
				    	<?php endwhile;
				    		sort($years);
				    	?>
				    		<ul class="event-filter">
				    			<li><a <?php if(!isset($_GET['filter'])){ echo ' class="active" '; } ?> href="/event/">All</a></li>
				    			<li><a <?php if(isset($_GET['filter']) && $filter == 'upcoming'){ echo ' class="active" '; } ?> href="/event/?filter=upcoming">Upcoming</a></li>
				    		<?php foreach ($years as $key => $year): ?>
								<li><a <?php if(isset($_GET['filter']) && $filter == $year){ echo ' class="active" ';} ?> href="/event/?filter=<?php echo $year; ?>"><?php echo $year; ?></a></li>
				    		<?php endforeach; ?>
							</ul>
				    	<div class="events pt-sm">
				    	<?php
				        while( $result->have_posts() ) : $result->the_post();
				        	$location = get_field('location');
				        	$date_type = get_field('date_type');
				        	$start_date = get_field('start_date', false, false);
				        	$end_date = get_field('end_date', false, false);
				        	$start_time = get_field('start_time');
				        	$end_time = get_field('end_time');
				        	$event_description = get_field('event_description');

				        	//Determin whether if it is an upcoming events
				        	$upcoming = false;

				        	date_default_timezone_set('UTC');
				        	$current_datetime = new DateTime();
				        	$start_datetime = new DateTime($start_date);	        	

				        	if($end_date){
				        		$end_datetime = new DateTime($end_date);
				        		$end_year = $end_datetime->format( 'Y' );
				        		$upcoming = $current_datetime <= $end_datetime;
				        	}else{
				        		$upcoming = $current_datetime <= $start_datetime;
				        	}
				        	//

				        	$start_year = $start_datetime->format( 'Y' );
				        	if(isset($_GET['filter'])){
				        		$filter = $_GET['filter'];
				        	}

				        	//Determin Whether if it is going to show
				        	$show = true;
				        	if(isset($_GET['filter']) && $filter == 'upcoming' && $upcoming != true){
				        		$show = false;
				        	}else if( isset($_GET['filter']) && 1900 < (int)$filter && (int)$filter < 2050){
				        		$year = $filter;
				        		
				        		if($year == $start_year || $year == $end_year){
				        			$show = true;
				        		}else{
				        			$show = false;
				        		}
				        	}

				        	//
				    	?>

						<?php if($show): ?>
				    		<div>
								<div class="date">
									<div class="start">
										<div class="day">
											<div class="day-of-week">
												<?php echo $start_datetime->format( 'D' ); ?>
											</div>
											<div class="day-of-month">
												<?php echo $start_datetime->format( 'd' ); ?>
											</div>
										</div>
										<div class="year-month">
											<?php echo $start_datetime->format( 'M' ); ?>
											<br>
											<?php echo $start_datetime->format( 'Y' ); ?>
										</div>
									</div>
									<?php if($end_date): ?>
									<div class="end">
										<div class="day">
											<div class="day-of-week">
												<?php echo $end_datetime->format( 'D' ); ?>
											</div>
											<div class="day-of-month">
												<?php echo $end_datetime->format( 'd' ); ?>
											</div>
										</div>
										<div class="year-month">
											<?php echo $end_datetime->format( 'M' ); ?>
											<br>
											<?php echo $end_datetime->format( 'Y' ); ?>
										</div>
									</div>
								<?php endif; ?>
								</div>
								<div class="content">
									<h3 class="h4"><?php the_title(); ?></h3>
									<div class="meta">
										<?php
											if($date_type == 'All Day' || $date_type == 'Varies'){
												echo $date_type;
											}else if($date_type == 'Time Range'){
												if( $start_time && $end_time ){
													echo $start_time . ' - ' . $end_time;
												}else if( !$start_time && $end_time ){
													echo 'Before ' . $end_time;
												}else if( $start_time && !$end_time ){
													echo 'After ' . $start_time;
												}
											}
										?>
										<br>
										<?php echo do_shortcode($location); ?>
									</div>
									<?php echo short_string($event_description, 10).'...'; ?>
									<a class="learn-more" href="<?php echo get_permalink(); ?>">Read More</a>
								</div>
				    		</div>
			    		<?php endif; ?>
						<?php
				        endwhile;
				        ?>
				        </div>
				        <?php
				    else:
				    	?>
						No event found.
				    	<?php
				    endif; // End Loop

				    wp_reset_query();
				?>
			</div>
		</div>

		</main>
	</div>
</div>
<?php get_footer();

<?php
/**
 * Default Page Template. Fixed width, no sidebar
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area career">
		<main id="main" class="site-main">

			<!-- Top Banner -->
			<?php get_template_part("/templates/template-parts/top-banner"); ?>
			<!-- end Top Banner -->

			<!-- Career -->
			<div class="career block pt-lg pb-lg main-content">
				<div class="container">
					<h1 class="h2">Career Opportunities</h1>
					<?php while ( have_posts() ) : the_post(); ?>
						<section>
							<h3 class="h3"><?php echo get_the_title(); ?></h3>
							<?php the_content(); ?>
						</section>
					<?php endwhile; ?>

					<div class="pt-md">
						Please send your resume and cover letter to Ms. Sue Newton at <?php echo do_shortcode('[lg-fax]'); ?> or <a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><?php echo do_shortcode('[lg-email]'); ?></a>
					</div>
				</div>
			</div>
			<!-- end Career -->

			<!-- Testimonials -->
			<?php get_template_part("/templates/template-parts/testimonials"); ?>
			<!-- end Testimonials -->

		</main>
	</div>
</div>
<?php get_footer();

<?php
/**
 * Default Page Template. Fixed width, no sidebar
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area team">
		<main id="main" class="site-main">

			<!-- Top Banner -->
			<?php get_template_part("/templates/template-parts/top-banner"); ?>
			<!-- end Top Banner -->

			<!-- Career -->
			<div class="pt-lg pb-lg">
				<div class="container">
					<div class="team-list">
					<?php while ( have_posts() ) : the_post(); 
						$title = get_the_title();
						$certificate = get_field('certificates');
						$occupation = get_field('occupation');
						$description = get_field('description');
					?>
						<section>
							<?php if(has_post_thumbnail()): ?>
							<div class="team-details">
								<div class="split-image">
									<?php the_post_thumbnail(); ?>
								</div>
								<div class="split-copy">
									<div class="team-heading">
										<h2 class="h4"><?php echo $title; ?> <span><?php if($certificate){ echo ', '.$certificate; } ?></span></h3>
										<?php if($occupation): ?>
											<span><?php echo $occupation; ?></span>
										<?php endif; ?>
									</div>
									<?php echo $description; ?>
								</div>
							</div>
							<?php else: ?>
								<div class="block">
									<div class="team-heading">
										<h2 class="h4"><?php echo $title; ?> <span><?php if($certificate){ echo ', '.$certificate; } ?></span></h3>
										<?php if($occupation): ?>
											<span><?php echo $occupation; ?></span>
										<?php endif; ?>
									</div>
									<?php echo $description; ?>
								</div>
							<?php endif; ?>
						</section>
					<?php endwhile; ?>
					</div>
					<div class="pt-md block">
						For more information about HUM’s policies, including cancellations and fees, please <a href="/policies-and-fees">click here</a>
					</div>
				</div>
			</div>
			<!-- end Career -->

			<!-- Testimonials -->
			<?php get_template_part("/templates/template-parts/testimonials"); ?>
			<!-- end Testimonials -->

		</main>
	</div>
</div>
<?php get_footer();

<?php
/**
 * Default Page Template. Fixed width, no sidebar
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area contact-us">
		<main id="main" class="site-main">

		<!-- Top Banner -->
		<?php get_template_part("/templates/template-parts/top-banner"); ?>
		<!-- end Top Banner -->

		<!-- Main Description -->
		<div class="intro-block block pt-lg pb-lg center bg-green">
			<div class="container">
				<?php echo term_description(); ?>
			</div>
		</div>
		<!-- end Main Description -->

		<!-- Archieve Section -->
		<?php
			$term = get_queried_object();
			$archieve_image = get_field('archieve_image', $term);
		?>
		<div class="service-archive block pt-lg pb-lg">
			<div class="container">
				<h1 class="h2 center"><?php single_term_title(); ?></h1>

				<?php
					$args = array(
			            'showposts'	=> -1,
			            'post_type'		=> 'service',
			            'tax_query' => array(
							array(
								'taxonomy' => 'service-category',
								'field'    => 'ID',
								'terms'    => array($term->term_id)
							),
						),
			        );
			        $result = new WP_Query( $args );

			        // Loop
			        if ( $result->have_posts() ) :
			        	?>
			        	<div class="services-list pt-xs">
			        		<div><img src="<?php echo $archieve_image['url']; ?>" alt="<?php echo $archieve_image['alt']; ?>"></div>
				        	<?php
				            while( $result->have_posts() ) : $result->the_post();
				            
				        	?>
				        		<div>
				        		<h3 class="h4"><?php the_title(); ?></h3>
								<p><?php echo short_string(get_the_content(), 40); ?></p>
								<a href="<?php echo get_permalink(); ?>">Read more</a>
				        		</div>
							<?php
				            endwhile;
				            ?>
			            </div>
			            <?php
			        endif; // End Loop

			        wp_reset_query();
				?>
			</div>
		</div>
		<!-- end Archieve Section -->

		<!-- Financing Section -->
		<?php get_template_part("/templates/template-parts/financing-available"); ?>
		<!-- end Financing Section -->

		<!-- Events -->
		<?php get_template_part("/templates/template-parts/upcoming-events"); ?>
		<!-- end Events -->
		
		</main>
	</div>
</div>
<?php get_footer();

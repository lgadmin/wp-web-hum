<?php
/**
 * Default Page Template. Fixed width, no sidebar
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area frequently-asked-questions">
		<main id="main" class="site-main">

			<!-- Top Banner -->
			<?php get_template_part("/templates/template-parts/top-banner"); ?>
			<!-- end Top Banner -->

			<!-- QA -->
			<div class="pt-lg pb-lg block">
				<div class="container">
					<h1 class="center">Frequently Asked Questions</h1>
					<?php
					if( have_rows('questions') ):
					    while ( have_rows('questions') ) : the_row();
					        $q = get_sub_field('q');
					        $a = get_sub_field('a');
					        ?>
							<section class="qa">
								<h3 class="h4"><?php echo do_shortcode($q); ?></h3>
								<p><?php echo do_shortcode($a); ?></p>
							</section>
					        <?php
					    endwhile;
					else :
					    // no rows found
					endif;
					?>
				</div>
			</div>
			<!-- end QA -->

			<!-- Testimonials -->
			<?php get_template_part("/templates/template-parts/testimonials"); ?>
			<!-- end Testimonials -->

		</main>
	</div>
</div>
<?php get_footer();

<?php
/**
 * Default Page Template. Fixed width, no sidebar
 */

get_header(); ?>
<div id="content" class="site-content">
  <div id="primary" class="content-area newsletter">
    <main id="main" class="site-main">

      <!-- Top Banner -->
      <?php get_template_part("/templates/template-parts/top-banner"); ?>
      <!-- end Top Banner -->

      <!-- Posts -->
      <div class="block pt-lg pb-lg">
        <div class="container">
      <?php
        //Protect against arbitrary paged values
        $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

        $args = array(
          'posts_per_page' => 4,
          'paged' => $paged,
          'orderby' => 'date',
          'order' => 'DESC'
        );

        $result = new WP_Query( $args );
        // Loop
        if ( $result->have_posts() ) : ?>
          <?php while( $result->have_posts() ) : $result->the_post(); 
            $video = get_field('video');
          ?>
            <?php if($video): ?>
              <div class="post-with-video">
                <div class="split-image">
                  <div class="post-heading">
                    <h2 class="h3"><?php the_title(); ?></h2>
                    <span>
                      Posted on <?php echo get_the_date(); ?>
                    </span>
                  </div>
                  <?php echo $video; ?>
                </div>
                <div class="split-copy">
                  <?php the_content(); ?>
                </div>
              </div>
            <?php else: ?>
              <div class="post-heading">
                <h2 class="h3"><?php the_title(); ?></h2>
                <span>
                  Posted on <?php echo get_the_date(); ?>
                </span>
              </div>
              <?php the_content(); ?>
            <?php endif; ?>
            <hr class="mt-md mb-md">
          <?php
            endwhile;
        endif; // End Loop

        wp_reset_query();
      ?>

      <?php
        $big = 999999999; // need an unlikely integer

        echo '<div class="center">';
        echo paginate_links( array(
          'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
          'format' => '?paged=%#%',
          'current' => max( 1, get_query_var('paged') ),
          'total' => $result->max_num_pages
        ) );
        echo '</div>';
      ?>
        </div>
      </div>
      <!-- end Posts -->

      <!-- Testimonials -->
      <?php get_template_part("/templates/template-parts/testimonials"); ?>
      <!-- end Testimonials -->

    </main>
  </div>
</div>
<?php get_footer();

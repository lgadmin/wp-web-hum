<?php
/**
 * Default Page Template. Fixed width, no sidebar
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area home">
		<main id="main" class="site-main">

		<!-- Featured Image -->
		<?php
			$banner_image = get_field('banner_image');
			$banner_headline = get_field('banner_headline');
			$banner_form = get_field('banner_form');
		?>
		<div class="home-banner">
			<img class="img-full" src="<?php echo $banner_image['url']; ?>" alt="<?php echo $banner_image['alt']; ?>">
			<div class="layers">
				<div class="container">
					<div class="headline">
						<?php if($banner_headline): ?>
							<h1 class="h1"><?php echo $banner_headline; ?></h1>
						<?php endif; ?>
					</div>
					<div class="form">
						<?php if($banner_form): ?>
							<?php echo do_shortcode($banner_form); ?>
						<?php endif; ?>
					</div>
					<div class="banner-cta">
						<a href="/contact-us/" class="cta h2">Get Help Now</a>
					</div>
				</div>
			</div>
		</div>
		<!-- end Featured Image -->

		<!-- Intro Section -->
		<?php
			$intro_section_title = get_field('intro_section_title');
			$intro_section_description = get_field('intro_section_description');
			$intro_section_slider = get_field('intro_section_slider');
		?>
		<div class="intro-block block pt-lg pb-lg bg-green center">
			<div class="container">
				<?php if($intro_section_title) :?>
					<h2 class="h2"><?php echo $intro_section_title; ?></h2>
				<?php endif; ?>
				<?php echo $intro_section_description; ?>
				<div class="intro-slider pt-md">
					<?php if($intro_section_slider): ?>
						<?php echo do_shortcode($intro_section_slider); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<!-- end Intro Section -->

		<!-- Counselling Services -->
		<?php
			$counselling_services_image = get_field('counselling_services_image');
			$counselling_services_title = get_field('counselling_services_title');
		?>
		<div class="counselling-services">
			<div class="split-image">
				<img src="<?php echo $counselling_services_image['url']; ?>" alt="<?php echo $counselling_services_image['alt']; ?>">
			</div>
			<div class="split-copy container">
				<?php if($counselling_services_title): ?>
					<h2 class="h2"><?php echo $counselling_services_title; ?></h2>
				<?php endif; ?>

				<?php
					$args = array(
			            'showposts'	=> 5,
			            'post_type'		=> 'service',
			            'tax_query' => array(
							array(
								'taxonomy' => 'service-category',
								'field'    => 'slug',
								'terms'    => array('counselling-service')
							),
						),
			        );
			        $result = new WP_Query( $args );

			        // Loop
			        if ( $result->have_posts() ) :
			        	$count = 0;
			        	?>
			        	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			        	<?php
			            while( $result->have_posts() ) : $result->the_post();
			            	$count++;
			        	?>
			        		<div class="panel panel-default">
							    <div class="panel-heading" role="tab" id="headingOne">
							        <h4 class="panel-title">
								        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#counselling-collapse-<?php echo $count; ?>" <?php if($count == 1){ echo 'aria-expanded="true"'; } ?> aria-controls="collapseOne">
								          <i class="fa fa-pencil-square-o" aria-hidden="true"></i><?php the_title(); ?>
								        </a>
							        </h4>
							    </div>
							    <div id="counselling-collapse-<?php echo $count; ?>" class="panel-collapse collapse <?php if($count == 1){ echo 'in'; } ?>" role="tabpanel" aria-labelledby="headingOne">
							        <div class="panel-body">
							        	<p><?php echo short_string(get_the_content(), 50); ?></p>
							        	<a class="read-more" href="<?php echo get_permalink(); ?>">Read more</a>
							        </div>
							    </div>
							 </div>
						<?php
			            endwhile;
			            ?>
			            </div>
			            <?php
			        endif; // End Loop

			        wp_reset_query();
				?>

				<a href="/service-category/counselling-service/" class="cta">View All Counselling Services</a>
			</div>
		</div>
		<!-- end Counselling Services -->

		<!-- Outpatient Program -->
		<?php
			$outpatient_program_title = get_field('outpatient_program_title');
			$outpatient_program_subtitle = get_field('outpatient_program_subtitle');
			$outpatient_program_description = get_field('outpatient_program_description');
			$outpatient_program_link = get_field('outpatient_program_link');
			$outpatient_program_image = get_field('outpatient_program_image');
		?>
		<div class="outpatient-program pt-lg pb-lg bg-gray-lighter">
			<div class="container">
				<div class="split-image">
					<img src="<?php echo $outpatient_program_image['url']; ?>" alt="<?php echo $outpatient_program_image['alt']; ?>">
				</div>
				<div class="split-copy">
					<?php if($outpatient_program_title): ?>
						<h2 class="h2"><?php echo $outpatient_program_title; ?><span><?php echo $outpatient_program_subtitle; ?></span></h2>
					<?php endif; ?>
					<?php echo $outpatient_program_description; ?>
					<a href="<?php echo $outpatient_program_link->guid; ?>" class="cta">Learn more</a>
				</div>
			</div>
		</div>
		<!-- end Outpatient Program -->

		<!-- Personal Services -->
		<?php
			$personal_services_image = get_field('personal_services_image');
			$personal_services_title = get_field('personal_services_title');
		?>
		<div class="personal-services pt-lg pb-lg">
			<div class="container">
				<?php if($personal_services_title): ?>
					<h2 class="h2 center"><?php echo $personal_services_title; ?></h2>
				<?php endif; ?>
				<div class="content pt-sm">
					<div class="split-image">
						<img src="<?php echo $personal_services_image['url']; ?>" alt="<?php echo $personal_services_image['alt'];?>">
					</div>
					<div class="split-copy">
						<?php
							$args = array(
					            'showposts'	=> 6,
					            'post_type'		=> 'service',
					            'tax_query' => array(
									array(
										'taxonomy' => 'service-category',
										'field'    => 'slug',
										'terms'    => array('personal-service')
									),
								),
					        );
					        $result = new WP_Query( $args );

					        // Loop
					        if ( $result->have_posts() ) :
					        	?>
					        	<div class="services">
					        	<?php
					            while( $result->have_posts() ) : $result->the_post();
					            
					        	?>
					        		<div>
					        		<h3 class="h3"><?php the_title(); ?></h3>
									<p><?php echo short_string(get_the_content(), 15); ?></p>
									<a href="<?php echo get_permalink(); ?>">Read more</a>
					        		</div>
								<?php
					            endwhile;
					            ?>
					            </div>
					            <?php
					        endif; // End Loop

					        wp_reset_query();
						?>

						<a href="/service-category/personal-service/" class="cta">View All Personal Services</a>
					</div>
				</div>
			</div>
		</div>
		<!-- end Personal Services -->

		<!-- Corporate Services -->
		<?php
			$corporate_services_title = get_field('corporate_services_title');
			$corporate_services_image = get_field('corporate_services_image');
		?>
		<div class="corporate-services pt-lg pb-lg bg-gray-lighter">
			<div class="container">
				<?php if($corporate_services_title): ?>
					<h2 class="h2 center"><?php echo $corporate_services_title; ?></h2>
				<?php endif; ?>
				<div class="content pt-sm">
					<div class="split-image">
						<img src="<?php echo $corporate_services_image['url']; ?>" alt="<?php echo $corporate_services_image['alt'];?>">
					</div>
					<div class="split-copy">
						<?php
							$args = array(
					            'showposts'	=> 6,
					            'post_type'		=> 'service',
					            'tax_query' => array(
									array(
										'taxonomy' => 'service-category',
										'field'    => 'slug',
										'terms'    => array('corporate-service')
									),
								),
					        );
					        $result = new WP_Query( $args );

					        // Loop
					        if ( $result->have_posts() ) :
					        	?>
					        	<div class="services">
					        	<?php
					            while( $result->have_posts() ) : $result->the_post();
					            
					        	?>
					        		<div>
					        		<h3 class="h3"><?php the_title(); ?></h3>
									<p><?php echo short_string(get_the_content(), 15); ?></p>
									<a href="<?php echo get_permalink(); ?>">Read more</a>
					        		</div>
								<?php
					            endwhile;
					            ?>
					            </div>
					            <?php
					        endif; // End Loop

					        wp_reset_query();
						?>

						<a href="/service-category/corporate-service/" class="cta">View All Corporate Services</a>
					</div>
				</div>
			</div>
		</div>
		<!-- end Corporate Services -->

		<!-- Testimonials -->
		<?php get_template_part("/templates/template-parts/testimonials"); ?>
		<!-- end Testimonials -->

		<!-- Events -->
		<?php get_template_part("/templates/template-parts/upcoming-events"); ?>
		<!-- end Events -->

		</main>
	</div>
</div>
<?php get_footer();

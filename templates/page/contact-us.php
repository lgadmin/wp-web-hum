<?php
/**
 * Default Page Template. Fixed width, no sidebar
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area contact-us">
		<main id="main" class="site-main">

		<!-- Top Banner -->
		<?php get_template_part("/templates/template-parts/top-banner"); ?>
		<!-- end Top Banner -->

		<!-- Contact Us -->
		<?php
			$title = get_field('title');
			$parking = get_field('parking');
			$form_title = get_field('form_title');
			$form_description = get_field('form_description');
			$form = get_field('form');
		?>
		<div class="contact container">
			<div class="split-copy">
				<?php if($title): ?>
					<h1 class="h2">Location and Contact Info</h1>
				<?php endif; ?>

				<section>
					Head Office : <span class="office"><?php echo do_shortcode('[lg-city]'); ?>, <?php echo do_shortcode('[lg-province]'); ?></span>
				</section>

				<section>
					<h2 class="h4">Address:</h2>
					<?php echo do_shortcode('[lg-address1]'); ?>, <?php echo do_shortcode('[lg-address2]'); ?><br>
					<?php echo do_shortcode('[lg-city]'); ?>, <?php echo do_shortcode('[lg-province]'); ?> <?php echo do_shortcode('[lg-postcode]'); ?>
				</section>

				<section>
					<h2 class="h4">Free lot parking:</h2>
					<?php echo $parking; ?>
				</section>

				<section>
					<div>Phone: <a href="tel:+1<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></a></div>
					<div>Fax: <?php echo format_phone(do_shortcode('[lg-fax]')); ?></div>
					<div>Email: <a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><?php echo do_shortcode('[lg-email]'); ?></a></div>
				</section>

				<section>
					<div class="logo">
						<?php
	    					if(shortcode_exists('lg-site-logo')){
	    						echo do_shortcode('[lg-site-logo]');
	    					}else{
	    						the_custom_logo();
	    					}
	    			 	?>
					</div>
				</section>
			</div>
			<div class="split-copy">
				<?php if($form_title): ?>
					<h2 class="h3"><?php echo $form_title; ?></h2>
				<?php endif; ?>
				<?php echo $form_description ?>
				<?php echo do_shortcode($form); ?>
			</div>
		</div>
		<!-- end Contact Us -->

		<!-- Google Map -->
		<?php
			$lg_map = get_field('lg_map');
		?>
		<div class="map">
			<?php echo do_shortcode($lg_map); ?>
		</div>
		<!-- end Google Map -->

		<!-- Events -->
		<?php get_template_part("/templates/template-parts/upcoming-events"); ?>
		<!-- end Events -->
		
		</main>
	</div>
</div>
<?php get_footer();

<?php
/**
 * Default Page Template. Fixed width, no sidebar
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area about-us">
		<main id="main" class="site-main">

			<!-- Top Banner -->
			<?php get_template_part("/templates/template-parts/top-banner"); ?>
			<!-- end Top Banner -->

			<!-- Who We Are -->
			<?php
				$who_we_are_title = get_field('who_we_are_title');
				$who_we_are_description = get_field('who_we_are_description');
				$who_we_are_video = get_field('who_we_are_video');
			?>
			<div class="who-we-are block pt-lg pb-lg bg-green">
				<div class="container">
					<div class="video">
						<?php echo $who_we_are_video; ?>
					</div>
					<?php if($who_we_are_title): ?>
						<h1 class="h2"><?php echo $who_we_are_title; ?></h1>
					<?php endif; ?>
					<?php echo $who_we_are_description; ?>
				</div>
			</div>
			<!-- end Who We Are -->

			<!-- Mission Statement -->
			<?php
				$mission_statement_description = get_field('mission_statement_description');
			?>
			<div class="bg-gray-lighter block pt-lg pb-lg center">
				<div class="container">
					<h2 class="h2">Our Mission Statement</h2>
					<?php echo $mission_statement_description; ?>
				</div>
			</div>
			<!-- end Mission Statement -->

			<!-- Meaning -->
			<?php
				$what_hum_means = get_field('what_hum_means');
				$trust_recovery = get_field('trust_recovery');
				$core_values = get_field('core_values');
			?>
			<div class="meaning block pt-lg pb-lg center">
				<div class="container">
					<div>
						<h3 class="h3">What HUM Means</h3>
						<?php echo $what_hum_means; ?>
					</div>
					<div class="trust-recovery">
						<h3 class="h3">Trust Recovery</h3>
						<div>
							<?php
								foreach ($trust_recovery as $key_word => $word) {
									?>
										<ul>
											<?php
												foreach ($word as $key_letter => $letter) {
													?>
														<li>
															<?php echo word_sepration($letter['word']); ?>
														</li>
													<?php
												}
											?>
										</ul>
									<?php
								}
							?>
						</div>
					</div>
					<div class="reach">
						<h3 class="h3">Core Values</h3>
						<div>
							<?php
								foreach ($core_values as $key_word => $word) {
									?>
										<ul>
											<?php
												foreach ($word as $key_letter => $letter) {
													?>
														<li>
															<?php echo word_sepration($letter['word']); ?>
														</li>
													<?php
												}
											?>
										</ul>
									<?php
								}
							?>
						</div>
					</div>
				</div>
			</div>
			<!-- end Meaning -->

			<!-- Testimonials -->
			<?php get_template_part("/templates/template-parts/testimonials"); ?>
			<!-- end Testimonials -->

			<!-- Events -->
			<?php get_template_part("/templates/template-parts/upcoming-events"); ?>
			<!-- end Events -->

		</main>
	</div>
</div>
<?php get_footer();

<?php
/**
 * Default Page Template. Fixed width, no sidebar
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area financial-assistance">
		<main id="main" class="site-main">

			<!-- Top Banner -->
			<?php get_template_part("/templates/template-parts/top-banner"); ?>
			<!-- end Top Banner -->

			<!-- Green Intro Section -->
			<?php
				$intro_text = get_field('intro_text');
			?>
			<div class="block bg-green intro center pt-lg pb-lg">
				<div class="container">
					<?php echo $intro_text; ?>
				</div>
			</div>
			<!-- end Green Intro Section -->

			<!-- Medicard -->
			<?php
				$medicard_image = get_field('medicard_image');
				$medicard_description = get_field('medicard_description');
			?>
			<div class="pt-lg pb-lg medicard">
				<div class="container">
					<div class="split-image">
						<img src="<?php echo $medicard_image['url']; ?>" alt="<?php echo $medicard_image['alt']; ?>">
					</div>
					<div class="split-copy">
						<?php echo $medicard_description; ?>
					</div>
				</div>
			</div>
			<!-- end Medicard -->

			<!-- FAQ -->
			<?php
				$faq_title = get_field('faq_title');
				$faq_blurb = get_field('faq_blurb');

				if( have_rows('faq') ):
					?>
					<div class="block faq pt-lg pb-lg bg-gray-lighter">
						<div class="container">
						<h3 class="h3"><?php echo $faq_title; ?></h3>
					<?php
				    while ( have_rows('faq') ) : the_row();
				        $q = get_sub_field('q');
				        $a = get_sub_field('a');
				        ?>
						<div class="qa-section">
							<div><div>Q:</div> <div><?php echo $q; ?></div></div>
							<div><div>A:</div> <div><?php echo $a; ?></div></div>
						</div>
				        <?php
				    endwhile;
				    ?>
				    	<div class="pt-sm">
				    		<?php echo do_shortcode($faq_blurb); ?>
				    	</div>
						</div>
					</div>
				    <?php
				else :
				    // no rows found
				endif;
			?>
			<!-- end FAQ -->

			<!-- Events -->
			<?php get_template_part("/templates/template-parts/upcoming-events"); ?>
			<!-- end Events -->

		</main>
	</div>
</div>
<?php get_footer();
